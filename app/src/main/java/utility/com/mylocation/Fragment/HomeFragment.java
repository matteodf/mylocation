package utility.com.mylocation.Fragment;

import android.location.Location;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import utility.com.mylocation.Entity.Luogo;
import utility.com.mylocation.LuogoAdapter;
import utility.com.mylocation.R;
import utility.com.mylocation.volley.AppController;

/**
 * Created by Matteo De Franceschi on 25/05/2017.
 */

public class HomeFragment extends Fragment {

    private ArrayList<Luogo> lista;
    private ListView listView;
    private LuogoAdapter adapter;
    private Spinner spinner;
    private Location currentPosition;

    public HomeFragment(ArrayList<Luogo> lista, Location currentPosition) {
        this.lista = lista;
        this.currentPosition = currentPosition;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.homefrag, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        spinner = (Spinner) view.findViewById(R.id.spinner);

        final List<String> list=new ArrayList<>();
        list.add("Ordina per rating");
        list.add("Ordina per distanza");
        ArrayAdapter<String> adp1=new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, list);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adp1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                switch (selected) {
                    case "Ordina per rating": {
                        Collections.sort(lista, new Comparator<Luogo>() {
                            @Override
                            public int compare(Luogo c1, Luogo c2) {
                                return Double.compare(c1.getRating(), c2.getRating());
                            }
                        });

                        lista = reverse(lista);
                    }break;
                    case "Ordina per distanza": {

                        for(int i = 0; i< lista.size(); i++) {

                            Location locationTemp = new Location("");
                            locationTemp.setLatitude(lista.get(i).getLatitude());
                            locationTemp.setLongitude(lista.get(i).getLongitude());
                            lista.get(i).setDistance(locationTemp.distanceTo(currentPosition) / 1000);
                            Log.d("distance from " + lista.get(i).getName(), "" + lista.get(i).getDistance());
                        }

                        Collections.sort(lista, new Comparator<Luogo>() {
                            @Override
                            public int compare(Luogo c1, Luogo c2) {
                                return Double.compare(c1.getDistance(), c2.getDistance());
                            }
                        });


                    }break;
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MapFagment mapFragment = new MapFagment(lista, position);
                FragmentTransaction ft2 = getActivity().getSupportFragmentManager().beginTransaction();
                ft2.replace(R.id.frame, mapFragment, "FragMap");
                ft2.commit();
            }
        });

        adapter = new LuogoAdapter(getActivity(), lista);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return view;
    }

    public ArrayList<Luogo> reverse(ArrayList<Luogo> list) {
        if(list.size() > 1) {
            Luogo value = list.remove(0);
            reverse(list);
            list.add(value);
        }
        return list;
    }
}