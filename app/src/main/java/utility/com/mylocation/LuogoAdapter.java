package utility.com.mylocation;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import utility.com.mylocation.Entity.Luogo;

import static utility.com.mylocation.R.id.ratingBar;

/**
 * Created by MatteoDe Franceschi on 25/05/2017.
 */

public class LuogoAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Luogo> lista;

    public LuogoAdapter(Activity activity, ArrayList<Luogo> lista) {
        this.activity = activity;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.cell_luogo, null);

        TextView name = (TextView) convertView.findViewById(R.id.txtname);
        TextView address = (TextView) convertView.findViewById(R.id.txtaddress);
        ImageView imgLuogo = (ImageView) convertView.findViewById(R.id.imgLuogo);
        RatingBar rating = (RatingBar) convertView.findViewById(ratingBar);

        Drawable progress = rating.getProgressDrawable();
        DrawableCompat.setTint(progress, Color.parseColor("#ffcc33"));

        Luogo lu = lista.get(position);

        name.setText(lu.getName());
        address.setText(lu.getAddress());
        rating.setRating((float) Double.parseDouble(String.valueOf(lu.getRating())));

        Picasso.with(convertView.getContext()).load("http://www.notifizzy.it/ITS/"+lu.getImg()).into(imgLuogo);

        return convertView;
    }
}
