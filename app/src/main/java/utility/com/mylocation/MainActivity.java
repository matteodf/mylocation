package utility.com.mylocation;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.MapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import utility.com.mylocation.Entity.Luogo;
import utility.com.mylocation.Fragment.HomeFragment;
import utility.com.mylocation.Fragment.MapFagment;
import utility.com.mylocation.Fragment.SettingsFragment;
import utility.com.mylocation.volley.AppController;

public class MainActivity extends AppCompatActivity implements MapFagment.IMap {

    private String URL_FEED = "http://www.notifizzy.it/ITS/POIjson.php";
    private static final String TAG = HomeFragment.class.getSimpleName();
    private ArrayList<Luogo> lista = new ArrayList<>();
    private HomeFragment homeFragment;
    private MapFagment mapFragment;
    private SettingsFragment settingsFragment;
    private Location currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                homeFragment = new HomeFragment(lista, currentPosition);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame, homeFragment, "Frag1");
                ft.commit();
            }
        }, 500);



//        Cache cache = AppController.getInstance().getRequestQueue().getCache();
//        Cache.Entry entry = cache.get(URL_FEED);
//        if (entry != null) {
//            // fetch the data from cache
//            try {
//                String data = new String(entry.data, "UTF-8");
//                try {
//                    parseJsonFeed(data);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//        } else {
            // making fresh volley request and getting json
            StringRequest jsonReq = new StringRequest(Request.Method.GET,
                    URL_FEED, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    VolleyLog.d(TAG, "Response: " + response);
                    parseJsonFeed(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
//        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        homeFragment = new HomeFragment(lista, currentPosition);
                        FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                        ft1.replace(R.id.frame, homeFragment, "FragHome");
                        ft1.commit();
                        return true;
                    case R.id.navigation_map:
                            mapFragment = new MapFagment(lista, -1);
                            FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();
                            ft2.replace(R.id.frame, mapFragment, "FragMap");
                            ft2.commit();
                        return true;
                    case R.id.navigation_settings:
                        settingsFragment = new SettingsFragment();
                        FragmentTransaction ft3 = getSupportFragmentManager().beginTransaction();
                        ft3.replace(R.id.frame, settingsFragment, "FragSettings");
                        ft3.commit();
                        return true;
                }
                return false;
            }
        });
    }

    private void parseJsonFeed(String response) {
        try {
            JSONArray feedArray = new JSONArray(response);
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                Luogo item = new Luogo();
                item.setId(feedObj.getInt("id"));
                item.setName(feedObj.getString("name"));
                item.setLatitude(feedObj.getDouble("latitude"));
                item.setLongitude(feedObj.getDouble("longitude"));
                item.setAddress(feedObj.getString("address"));
                // Image might be null sometimes
                String image = feedObj.getString("img_url");
                item.setImg(image);
                item.setRating(feedObj.getDouble("rating"));
                lista.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getCurrentPosition(Location lastLoc) {
        currentPosition = lastLoc;
    }
}
